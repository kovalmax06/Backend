package com.example.tests;

import com.example.BaseTest;
import com.example.steps.BaseSteps;
import com.example.steps.BikeParkingPageSteps;
import com.example.steps.DescriptionPageSteps;
import org.testng.annotations.Test;
import com.codeborne.selenide.Selenide;

public class DescriptionPageTest extends BaseTest {
    //iit-33
    @Test(groups = "checkDescription")
    public void checkDescription()
    {
        BaseSteps baseSteps = new BaseSteps();
        baseSteps.Precondition();

        BikeParkingPageSteps bikePageSteps = new BikeParkingPageSteps();
        Selenide.switchTo().window(1);
        bikePageSteps.selectCategory("Описание");

        DescriptionPageSteps descriptionPageSteps = new DescriptionPageSteps();
        descriptionPageSteps.checkDescription();
    }
}
