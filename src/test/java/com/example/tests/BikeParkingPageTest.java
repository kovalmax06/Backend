package com.example.tests;

import com.example.BaseTest;
import com.example.steps.BaseSteps;
import com.example.steps.BikeParkingPageSteps;
import org.hamcrest.Matcher;
import org.testng.annotations.Test;
import com.codeborne.selenide.Selenide;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.startsWith;import org.testng.annotations.DataProvider;
import com.codeborne.selenide.testng.annotations.Report;

public class BikeParkingPageTest extends BaseTest {


    //iit-31
    @Test(groups = "test")
    public void choiceCategory ()
    {
        BaseSteps baseSteps = new BaseSteps();
        baseSteps.Precondition();

        BikeParkingPageSteps bikePageSteps = new BikeParkingPageSteps();

        Selenide.switchTo().window(1);
        bikePageSteps.enterAddress("Алтуфьевское")
                     .checkSearchOutputTable("Алтуфьевское");
    }
}
