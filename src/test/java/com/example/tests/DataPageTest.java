package com.example.tests;

import com.example.BaseTest;
import com.example.steps.*;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;import static org.hamcrest.Matchers.startsWith;import static org.hamcrest.Matchers.anyOf;

public class DataPageTest extends BaseTest {

    @DataProvider(name = "search")
    public Object[][] createData1() {
        return new Object[][] {
                { "Парковки такси"},
                { "Велопарковки"},
        };
    }

    //iit-30
    @Test(groups = "search", dataProvider = "search")
    public void search(String searchStr)  {
        MainPageSteps mainPageSteps = new MainPageSteps();

        mainPageSteps.openPage()
                .getPage().shouldBeOpened();
        mainPageSteps.selectCategory("Данные");

        DataPageSteps dataPageSteps = new DataPageSteps();
        dataPageSteps.getPage().shouldBeOpened();
        dataPageSteps.checkSearchInputPlaceholder("Поиск по названию")
                .searchFor(searchStr)
                .checkSearchOutputText(searchStr, "Отсутствуют датасеты, согласно заданным условиям");
    }
}
