package com.example.tests;

import com.example.BaseTest;
import com.example.steps.BaseSteps;
import com.example.steps.BikeParkingPageSteps;
import com.example.steps.PassportPageSteps;
import org.testng.annotations.Test;
import com.codeborne.selenide.Selenide;

public class PassportPageTest extends BaseTest {
    //iit-34
    @Test(groups = "checkPassport")
    public void checkPassport()
    {
        BaseSteps baseSteps = new BaseSteps();
        baseSteps.Precondition();

        BikeParkingPageSteps bikePageSteps = new BikeParkingPageSteps();
        Selenide.switchTo().window(1);
        bikePageSteps.selectCategory("'Паспорт'");

        PassportPageSteps passportPageSteps = new PassportPageSteps();
        passportPageSteps.checkDatasetName("Велопарковки");
    }
}
