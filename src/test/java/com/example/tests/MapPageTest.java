package com.example.tests;

import com.codeborne.selenide.Selenide;
import com.example.BaseTest;
import com.example.steps.BaseSteps;
import com.example.steps.BikeParkingPageSteps;
import com.example.steps.MapPageSteps;
import org.testng.annotations.Test;


public class MapPageTest extends BaseTest {
    //iit-32
    @Test(groups = "checkMap")
    public void checkMap()
    {
        BaseSteps baseSteps = new BaseSteps();
        baseSteps.Precondition();

        BikeParkingPageSteps bikePageSteps = new BikeParkingPageSteps();
        Selenide.switchTo().window(1);
        bikePageSteps.selectCategory("Карта");

        MapPageSteps mapPageSteps = new MapPageSteps();
        mapPageSteps.openPage()
                .selectCircle("div", "5")
                .selectCircle("img", "1")
                .checkAddressLandmark("Москва, поселение Внуковское, поселок Минвнешторга, улица Ленина, 2");

    }


}
