package com.example.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class PassportPage extends AbstractPage {

    public PassportPage() {
        super();
        this.url = "https://data.mos.ru/opendata/7704786030-veloparkovki/passport?versionNumber=5&releaseNumber=8";
    }

    public AbstractPage navigate() {
        return super.navigate(this.getClass());
    }

    @Override
    public AbstractPage waitPageLoaded() {
        $(By.className("page")).waitUntil(visible, 90000);
        return this;
    }

    public SelenideElement getCircle()
    {
          return $(By.xpath("//*[@id=\"datasetMap\"]/div[1]/div[2]/div[3]/div[3]/div"));
    }

    public SelenideElement getDatasetNameClass()
    {
        return $(By.xpath("//*[@id=\"passport\"]/article/table/tbody/tr[3]/td[2]"));
    }
}
