package com.example.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class DescriptionPage extends AbstractPage {

    public DescriptionPage() {
        super();
        this.url = "https://data.mos.ru/opendata/7704786030-veloparkovki/description?versionNumber=5&releaseNumber=8";
    }

    public AbstractPage navigate() {
        return super.navigate(this.getClass());
    }

    @Override
    public AbstractPage waitPageLoaded() {
        $(By.className("page-container")).waitUntil(visible, 90000);
        return this;
    }

    public SelenideElement getDescription()
    {
        return $("#description");
    }
}
