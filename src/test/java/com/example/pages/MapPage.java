package com.example.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class MapPage extends AbstractPage {

    public MapPage() {
        super();
        this.url = "https://data.mos.ru/opendata/7704786030-veloparkovki/data/map?versionNumber=5&releaseNumber=8";
    }

    public AbstractPage navigate() {
        return super.navigate(this.getClass());
    }

    @Override
    public AbstractPage waitPageLoaded() {
        $(By.className("page")).waitUntil(visible, 90000);
        return this;
    }

    public SelenideElement getCircle(String kind, String num)
    {
        return $("#datasetMap > div.leaflet-map-pane > div.leaflet-objects-pane > div.leaflet-marker-pane >"+ kind + ":nth-child(" + num + ")");
    }

 public SelenideElement getAddress()
 {
     return $("#card > div > div.container.scroll");
 }
}
