package com.example.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class BikeParkingPage extends AbstractPage {

    public BikeParkingPage() {
        super();
        this.url = "https://data.mos.ru/opendata/7704786030-veloparkovki";
    }

    public AbstractPage navigate() {
        return super.navigate(this.getClass());
    }

    @Override
    public AbstractPage waitPageLoaded() {
        $(By.className("table-absolute")).waitUntil(visible, 30000);
        return this;
    }

    public SelenideElement getAddressField()
    {
        return $("input[data-col-name = Address]");

    }


    public SelenideElement getSearchClass()
    {
        return $("#rows-content > tbody");
    }
}
