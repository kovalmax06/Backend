package com.example.steps;


import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Condition.text;
import com.example.pages.DescriptionPage;

public class DescriptionPageSteps extends BaseSteps<DescriptionPage> {
    public DescriptionPageSteps() {
        page = new DescriptionPage();
    }

    public DescriptionPageSteps openPage() {
        page.navigate().shouldBeOpened();
        return this;
    }

    public DescriptionPageSteps checkDescription()
    {
        getPage().getDescription().shouldHave(text("Набор данных содержит подробную информацию о специально оборудованных местах для размещения велосипедного транспорта и посмотреть их местоположение на карте."));
        return this;
    }
}
