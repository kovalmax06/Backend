package com.example.steps;

import com.codeborne.selenide.SelenideElement;
import com.example.pages.DataPage;


import static com.codeborne.selenide.Condition.*;


public class DataPageSteps extends BaseSteps<DataPage> {
    public DataPageSteps() {
        page = new DataPage();
    }

    public DataPageSteps openPage() {
        page.navigate().shouldBeOpened();
        return this;
    }

    public DataPageSteps checkSearchInputPlaceholder(String expectedPlaceholder) {
        SelenideElement searchField = page.getSearchField();
        searchField.shouldHave(attribute("placeholder", expectedPlaceholder));
        return this;
    }

    public DataPageSteps searchFor(String searchText) {
        SelenideElement searchField = page.getSearchField();
        searchField.setValue(searchText);
        page.getSearchButton().click();
        return this;
    }

    public DataPageSteps checkSearchOutputText(String expectedValue, String unexpectedValue) {
        getPage().getSearchClass().shouldHave(text(expectedValue));
        getPage().getSearchClass().shouldNotHave(text(unexpectedValue));
        return this;
    }

    public DataPageSteps selectCategory(String name)
    {
        SelenideElement href = page.getHref(name);
        href.click();
        return this;
    }

}
