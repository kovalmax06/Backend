package com.example.steps;


import com.codeborne.selenide.SelenideElement;
import com.example.pages.PassportPage;

import static com.codeborne.selenide.Condition.text;

public class PassportPageSteps extends BaseSteps<PassportPage> {

    public PassportPageSteps() {
        page = new PassportPage();
    }

    public PassportPageSteps openPage() {
        page.navigate().shouldBeOpened();
        return this;
    }

    public PassportPageSteps selectCircle(){
        SelenideElement circle = page.getCircle();
        circle.click();
        return this;
    }

    public PassportPageSteps checkDatasetName(String expectedValue) {
        getPage().getDatasetNameClass().shouldHave(text(expectedValue));
        return this;
    }


}
