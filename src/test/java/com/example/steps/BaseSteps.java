package com.example.steps;


public class BaseSteps<T> {
    T page;

    public T getPage() {
        return page;
    }

    public void setPage(T page) {
        this.page = page;
    }

    public  void Precondition()
    {
        MainPageSteps mainPageSteps = new MainPageSteps();
        mainPageSteps.openPage()
                .selectCategory("Дороги и транспорт");

        DataPageSteps categoryPageSteps = new DataPageSteps();
        categoryPageSteps.openPage()
                .selectCategory("Дороги и транспорт")
                .selectCategory("Велопарковки");
    }
}
