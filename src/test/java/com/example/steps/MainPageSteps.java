package com.example.steps;

import com.codeborne.selenide.SelenideElement;
import com.example.pages.MainPage;

import static com.codeborne.selenide.Condition.attribute;

public class MainPageSteps extends BaseSteps<MainPage> {


    public MainPageSteps() {

        page = new MainPage();

    }

    public MainPageSteps openPage() {
        page.navigate().shouldBeOpened();
        return this;
    }

    public MainPageSteps selectCategory(String name){
        SelenideElement dataHref = page.getHref(name);
        dataHref.click();
        return this;
    }
}
