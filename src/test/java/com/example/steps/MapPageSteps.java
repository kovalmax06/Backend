package com.example.steps;

import com.codeborne.selenide.SelenideElement;
import com.example.pages.MapPage;

import static com.codeborne.selenide.Condition.text;

public class MapPageSteps extends BaseSteps<MapPage> {

    public MapPageSteps() {
        page = new MapPage();
    }

    public MapPageSteps openPage() {
        page.navigate().shouldBeOpened();
        return this;
    }

    public MapPageSteps selectCircle(String kind, String num)
    {
        SelenideElement circle = page.getCircle(kind, num);
        circle.click();
        return this;
    }

    public MapPageSteps checkAddressLandmark(String address)
    {
        getPage().getAddress().shouldHave(text(address));
        return this;
    }

}
