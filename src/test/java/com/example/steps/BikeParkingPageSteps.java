package com.example.steps;

import com.codeborne.selenide.SelenideElement;
import com.example.pages.BikeParkingPage;

import static com.codeborne.selenide.Condition.text;


public class BikeParkingPageSteps extends BaseSteps<BikeParkingPage> {

    public BikeParkingPageSteps() {
        page = new BikeParkingPage();
    }


    public BikeParkingPageSteps enterAddress(String searchText){
        SelenideElement searchField = page.getAddressField();
        searchField.setValue(searchText);
        return this;
    }

    public BikeParkingPageSteps checkSearchOutputTable(String expectedValue) {
        getPage().getSearchClass().shouldHave(text(expectedValue));
        return this;
    }


    public BikeParkingPageSteps selectCategory(String name)
    {
        SelenideElement href = page.getHref(name);
        href.click();
        return this;
    }


}
